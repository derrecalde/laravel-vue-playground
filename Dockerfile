FROM php:7.4-apache

RUN a2enmod rewrite
#zlib1g-dev libicu-dev wget gnupg g++ git openssh-client libpng-dev iproute2
RUN apt-get update \
  && apt-get install -y g++ libicu-dev libzip-dev zlib1g-dev libpng-dev \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl pdo_mysql zip gd

RUN pecl install -f xdebug && docker-php-ext-enable xdebug;

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update \
  && apt-get install -y nodejs \
  && apt-get install -y yarn

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
