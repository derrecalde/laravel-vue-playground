
## Get Started
* Set up the .env.example as .env file
* Run Docker compose ```docker-compose up -d```
* Install dependencies ```docker exec -i laravel-php-vue composer install && npm install```
* Set up required key ```docker exec -i laravel-php-vue php artisan key:generate```
* Set up required tables database ```docker exec -i laravel-php-vue php artisan migrate```
* Check on http://localhost:8000

## Create a User
* From php container ```docker exec -it laravel-php-vue bash```    

#### Require to set passport credentials for Auth
* ```php artisan passport:install```   
* Fill the .env file with the **grant client** ID & secret   
> PASSPORT_CLIENT_ID=2   
> PASSPORT_CLIENT_SECRET=DTfy8************fefrefU   

#### Generate a user

* Generate a random user (login = email & default password = 'password')    
```php artisan tinker``` then ```factory('App\User')->create()```
* Keep email generated on a side   
* Connect on http://localhost:8000/#/login   

My generated is (bode.easter@example.net)

#### Any vue modifications require
* Compile vue from php container    
```npm run watch``` or ```npm run dev```   

## Urls

### Local
* Front : http://localhost:8000   
* PhpMyadmin : http://localhost:8081 (access in .env : MSQL)   


## Docker
### Required files   
* docker/*   
* Dockerfile   
* docker-copose.yml (local use cases)   

Run ```docker-compose up --build -d```    
Stop ```docker-compose stop```    
Remove volumes (BDD) ```docker-compose down -v```    

## PHP
* routes are in routes/web.php   

Run php commands line inside php container   

```docker exec -it laravel-php-vue bash``` then ```any command lines...```   

## Vue

*  Located inside ressources/js directory   

from php container, run ```npm run watch``` or ```npm run dev```to compile vu je files   

## Auth passport

[Passport auth](https://laravel.com/docs/7.x/passport)   

### Request token login example   
#### Requirements
* Type : Post 
* Send params as form (Simulate in Postamn : Boby > form-data)

**Endpoint** : [](http://localhost:8000/oauth/token)   

### Params 
* **grant_type** : **password**   
* **client_id** : **2** (Table : oauth_clients > id : where providers = users )   
* **client_secret** : **QYX8FshpIA3ftb9QlTmp2U2kG7Edsy4cOLrwoQW9** (Table : oauth_clients > secret : where providers = users )   
* **username** : **walker.willard@example.net** ( Table : users > email )   
* **password** : **password** (default Laravek generated password = 'password')   

Return {"access_token" : "edezd......dezU"}

### Use the Access_token login 
#### Requirements
* Type : get   
**Endpoint** : [](http://localhost:8000/api/user)   

* Set headers : "Accept = application/json"  (Postman : Headers panel)
* Set as Bearer Token  = "edezd......dezU"  (Postman : Authorization panel)


--------------

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
