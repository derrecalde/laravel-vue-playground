<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/** Items **/
// Call the index() function from Http/Controllers/ItemController.php
Route::get('/itemsList', 'ItemController@index');
// Call in post the store function
Route::post('/itemsList', 'ItemController@store');
// Call in post the destroy function
Route::post('/destroyItem/{id}', 'ItemController@destroy');
// Call in post the update function
Route::post('/updateItem/{id}', 'ItemController@update');

/** Skills **/
// Call the index() function from Http/Controllers/SkillController.php
Route::get('/skillsList', 'SkillController@index');
// Call in post the store function
Route::post('/skillsList', 'SkillController@store');
// Call in post the destroy function
Route::post('/destroySkill/{id}', 'SkillController@destroy');
// Call in post the update function
Route::post('/updateSkill/{id}', 'SkillController@update');

/** About **/
// Call the index() function from Http/Controllers/AboutController.php
Route::get('/about', 'AboutController@index');
// Call in post the store function
// Route::post('/aboutData', 'AboutController@store');
// Call in post the destroy function
// Route::post('/destroyAbout/{id}', 'AboutController@destroy');
// Call in post the update function
Route::post('/updateAbout/{id}', 'AboutController@update');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
