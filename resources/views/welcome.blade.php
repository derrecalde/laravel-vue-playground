<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Social icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="{{ env('APP_URL').'/css/app.css' }}" rel="stylesheet" >

        <!-- w3Scholl Template -->
        <!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
        <style>
            body,h1,h2,h3,h4,h5 {font-family: "Nunito", sans-serif}
            a:hover{text-decoration:none;}
            .w3-quarter{cursor: pointer}
            /* .w3-quarter img{margin-bottom: -6px; cursor: pointer} */
            /* .w3-quarter img:hover{opacity: 0.6; transition: 0.3s} */
            .w3-quarter:hover{opacity: 0.6; transition: 0.3s}
            .w3-quarter {
                height: 350px;
                background-size: cover;
                background-position: center;
            }
            .w3-bar .active {
                background-color: black;
                color: white;
            }

            .gallery-enter-active, .gallery-leave-active{
                transition: opacity 0.4s
            }
            .gallery-enter, .gallery-leave-to{
                opacity: 0;
            }
            .loader{
                background-color: #172028;
                text-align: center;
                min-height:150px;
                color: white;
            }
            .btn-socials{
                display: flex;
                justify-content: space-evenly;
            }
            .btn-socials .btn{
                color: darkgrey;
                font-size:24px;
            }
            .login-content{
              padding-top: 10%;
            }
            #about{
              background-image: url(/images/wood.jpg);
              background-size: cover;
            }

        </style>

    </head>
    <body class="w3-light-grey" >
        <div id="app">


            <!-- Sidebar/menu -->
            <nav class="w3-sidebar w3-bar-block w3-black w3-animate-right w3-top w3-text-light-grey w3-large" style="z-index:3;width:250px;font-weight:bold;display:none;right:0;" id="mySidebar">
                <a href="javascript:void()" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-32">X</a>
                <router-link to="/" class="w3-bar-item w3-button w3-center w3-padding-16" >HOME</router-link>
                <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-16">A PROPOS</a>
                <router-link to="/admin" class="w3-bar-item w3-button w3-center w3-padding-16" >admin</router-link>
            </nav>

            <!-- Top menu on small screens -->
            <header class="w3-container w3-top w3-white w3-xlarge w3-padding-16">
            <span class="w3-left w3-padding">PORTFOLIO</span>
            <a href="javascript:void(0)" class="w3-right w3-button w3-white" onclick="w3_open()">☰</a>
            </header>

            <!-- Overlay effect when opening sidebar on small screens -->
            <div class="w3-overlay w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

            <!-- !PAGE CONTENT! -->
            <router-view></router-view>

        </div>

        <script src="{{ env('APP_URL').'/js/app.js' }}" ></script>

        <script>
        // Script to open and close sidebar
        function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
        }

        function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
        }

        // Modal Image Gallery
        function onClick(element) {
        document.getElementById("img01").src = element.getAttribute('data-src');
        document.getElementById("modal01").style.display = "block";
        var captionText = document.getElementById("caption");
        captionText.innerHTML = element.getAttribute('data-alt');
        }

        </script>

    </body>
</html>
