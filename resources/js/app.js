/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/** Import components as tag html **/
// Admin components
Vue.component('add-item', require('./components/AddItemComponent.vue').default);
Vue.component('update-item', require('./components/UpdateItemComponent.vue').default);
Vue.component('img-uploader', require('./components/ImageUploaderComponent.vue').default);
Vue.component('destroy-item', require('./components/DestroyItemComponent.vue').default);
Vue.component('items', require('./components/ItemComponent.vue').default);

Vue.component('skill', require('./components/SkillComponent.vue').default);
Vue.component('about-me', require('./components/UpdateAboutComponent.vue').default);

Vue.component('add-skill', require('./components/AddSkillComponent.vue').default);
Vue.component('destroy-skill', require('./components/DestroySkillComponent.vue').default);

Vue.component('logout-btn', require('./components/LogoutComponent.vue').default);

// Front components
Vue.component('gallery', require('./components/GalleryComponent.vue').default);
Vue.component('about', require('./components/AboutComponent.vue').default);
Vue.component('skills', require('./components/SkillsComponent.vue').default);
Vue.component('contact', require('./components/ContactComponent.vue').default);
Vue.component('about-footer', require('./components/AboutFooterComponent.vue').default);

/* -- Import librabies -- */
import VueParallaxJs from 'vue-parallax-js'
Vue.use(VueParallaxJs);

/***  Routing  ***/
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/** vue-toastr (for notification console.log) **/
import Toastr from 'vue-toastr';
Vue.use(Toastr);

/*-- Import component  for destinations routes --*/
import Home from './components/HomeComponent.vue';
import Admin from './components/AdminComponent.vue';
import Login from './components/LoginComponent.vue';


/** Private routes */


/*-- Setting Routes components --*/
const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/admin',
        component: Admin,
        meta: {
            requiresAuth: true // Set the protected route
        }
    },
    {
        path: '/login',
        component: Login,
        meta: {
            requiresVisitor: true // Set the protected route
        }
    }
];

const router = new VueRouter({routes}); // assign routes conf


// Protected routes
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // Make /item not available if not logged in
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if ( !localStorage.getItem('access_token') ) {
        console.log('Not login ');
        next({
          path: '/login'
        })
      } else {
          console.log('Login !! ');
        next()
      }

    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    // Make /login not available if logged in
    // this route requires auth, check if logged in
    // if logged in, redirect to home page.
    if ( localStorage.getItem('access_token') ) {
        console.log('Not login ');
        next({
        path: '/'
        })
    } else {
        console.log('Login !! ');
        next()
    }

    } else {
      next() // make sure to always call next()!
    }
  })

// Vue.component('home-component', require('./components/HomeComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router: router // enable router in vue
});
