<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**  
 * Require 'composer require guzzlehttp/guzzle'
 * 
 * Doc : https://laravel.com/docs/7.x/http-client
 * https://www.youtube.com/watch?v=HGh0cKEVXPI
 * 
 */
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        try{

            // http://localhost instead http://localhost:8000 from Docker container
            $response = Http::post(env("APP_URL", "http://localhost").'/oauth/token', [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
            ]);
            return $response->getBody();

        } catch(\GuzzleHttp\Exception\BadResponseEception $e){

            return response()->json('Error : ', $e->getCode());

        }

    }

    /**
    * Logout
    */
    public function logout()
    {
        auth()->user()->tokens->each(
            function ($token, $key){
                $token->delete();
            });
        return response()->json('Logout success !', 200);
    }
}
