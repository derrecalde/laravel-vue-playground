<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = About::orderBy('id', 'desc')->first();// get() return array & first return an signle object
        return response()->json($about);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadedFile = $this->uploadFile($request);
        if ( $uploadedFile ){

            $request->merge(['img' => $uploadedFile]);
            $store = About::create($request->all());

            if( $store ){
                return $this->index();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Upload a file
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadFile($request)
    {
        if ($request->file('file') == null) {
            $file = "";
        } else {
           $file = $request->file('file')->store('images');
        }

        return $file; // generated path file
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = $this->getAbout($id);
        $uploadedFile = $this->uploadFile($request);
        // File changed
        if( $uploadedFile && $about->img != $uploadedFile ){
            Storage::delete($about->img);
            $about->img = $uploadedFile;
        }

        $about->title = $request->title;
        $about->description = $request->description;
        $about->infos = $request->infos;
        $about->permis = $request->permis;
        $about->hobbies = $request->hobbies;

        $status = $about->save();

        if( $status ){
            return $this->index();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = $this->getAbout($id);

        if( $about ){
            Storage::delete($about->img);
            About::where('id', '=', $id)->delete();
        }

        return $this->index();
    }

    /**
     * Show default items list
     * 
     * @return \Illuminate\Http\Response
     */
    private function getAbout($id){

        // $request = Item::orderBy('id', 'DESC')->get();
        return About::find($id);

    }
}
