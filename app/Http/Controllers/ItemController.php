<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $items = Item::all();
        // return response()->json($items);

        return $this->getItems();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadedFile = $this->uploadFile($request);
        if ( $uploadedFile ){

            $request->merge(['img' => $uploadedFile]);
            $store = Item::create($request->all());

            if( $store ){
                return $this->getItems();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadFile($request)
    {
        if ($request->file('file') == null) {
            $file = "";
        } else {
           $file = $request->file('file')->store('images');
        }

        return $file; // generated path file
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = $this->getItem($id);
        $uploadedFile = $this->uploadFile($request);
        // File changed
        if( $uploadedFile && $item->img != $uploadedFile ){
            Storage::delete($item->img);
            $item->img = $uploadedFile;
        }

        $item->name = $request->name;
        $item->description = $request->description;
        
        $status = $item->save();

        if( $status ){
            return $this->getItems();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->getItem($id);

        if( $item ){
            Storage::delete($item->img);
            Item::where('id', '=', $id)->delete();
        }

        return $this->getItems();

    }

    /**
     * Show default items list
     * 
     * @return \Illuminate\Http\Response
     */
    private function getItems(){

        $request = Item::orderBy('created_at', 'DESC')->get();

        return response()->json($request);
    }

    /**
     * Show default items list
     * 
     * @return \Illuminate\Http\Response
     */
    private function getItem($id){

        // $request = Item::orderBy('id', 'DESC')->get();
        return Item::find($id);

    }
}
