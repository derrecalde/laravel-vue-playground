<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    // Allow Laravel to fill name field in DDB
    protected $fillable = ['title', 'description', 'img'];
}
