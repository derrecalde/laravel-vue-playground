<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    // Allow Laravel to fill name field in DDB
    protected $fillable = ['name', 'value', 'order'];
}
