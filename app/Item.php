<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    // Allow Laravel to fill name field in DDB
    protected $fillable = ['name', 'description', 'img'];
}
